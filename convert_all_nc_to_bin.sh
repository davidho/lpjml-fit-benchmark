#!/bin/bash

# Define the command path
directory="$1"
cdf2bin_path="/p/projects/landuse/users/davidho/LPJmL/LPJmLVersions/debug_comp/master/LPJmL_internal/bin/cdf2bin"
cdf2grid_path="/p/projects/landuse/users/davidho/LPJmL/LPJmLVersions/debug_comp/master/LPJmL_internal/bin/cdf2grid"

# Create directory
mkdir "$directory/bin_files"

# Copy terr_area from lpjml run 
cp "/p/projects/landuse/users/davidho/LPJmL/simulations/output/new_heatcond_5.9/terr_area.bin" "$directory/bin_files"
cp "/p/projects/landuse/users/davidho/LPJmL/simulations/output/new_heatcond_5.9/terr_area.bin.json" "$directory/bin_files"

# Find any file with "grid[...].nc" pattern in the directory
echo "convert grid..."
grid_file=$(find "$directory" -name 'grid*.nc')

# Convert grid
$cdf2grid_path -json -raw "$grid_file" "$directory/bin_files/grid.bin"
echo "grid converted"


# Loop over all .nc files in the current directory
for nc_file in "$directory"/*.nc; 
do
    # Extract the base name of the .nc file (without extension)
    # ${nc_file%.nc} removes the .nc suffix from the filename
    base_name="$(basename "$nc_file" .nc)"

    # Remove '_global' suffix if it exists
    base_name="${base_name%_global}"

    if [ "$base_name" != "grid" ]; then
        # Run the command with the base name as the output option and the appropriate flag if set
        $cdf2bin_path -o "$directory/bin_files/$base_name.bin" -json "$directory/bin_files/grid.bin" "$nc_file"
    fi
    
    echo "Processed $nc_file"
done

echo "All .nc files processed."