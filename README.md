# lpjml-FIT-benchmark
To use lpjmlstats benchmarking for lpjmlfit, follow these steps:
1. Use the bash script ```convert_all_nc_to_bin.sh```  like this 
```
convert_all_nc_to_bin.sh path/to/fit/output/dir
```
to create a subfolder bin_files in an output dir that contains bin versions of the lpjmlfit netcdf outputs.

2. load the piam module with 
```
source /p/system/modulefiles/defaults/piam/module_load_piam
```

3. Possibly you need to unset local packages by executing ```liboff```. (After benchmarking remember to turn on local packages again with ```libon```)

4. Run ```R``` to start R

5. Run ```library(lpjmlstats)``` in R

6. Source ```lpjmlfit_config.R``` with 
```
source("path/to/this/repository/lpjmlfit_config.R")
```

7. Run ```?benchmark``` to learn about how to use the benchmarking function.

8. Start a benchmark with some example settings for lpjml-FIT
```
benchmark("path/to/baseline/output/bin_files", 
          "path/to/under_test/output/bin_files", 
          settings = fit_settings,
          metric_options = fit_metric_ops
          )
```

9. Explore what is possible by going through the vignettes 
https://gitlab.pik-potsdam.de/lpjml/lpjmlstats/-/blob/main/vignettes/benchmark.Rmd?ref_type=heads
https://gitlab.pik-potsdam.de/lpjml/lpjmlstats/-/blob/main/vignettes/benchmark-change-settings.Rmd 